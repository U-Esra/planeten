package main

import (
	"fmt"
	)

func main () {
	km := 150000000
	u := 8766
	kmu := km / u
//  kmu := float64(km) / float64(u) voor kommagetal
	fmt.Println("De planeet staat", km, "km van haar ster en draait een rondje in", u, "uren, dus beweegt zich door de ruimte met", kmu, "km per uur.")
}
